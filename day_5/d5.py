
class Crane:
    def __init__(self, size: int):
        self.size = size
        self.dock = [[] for _ in range(self.size)]

    def move_from_to(self, c_from: int, c_to: int):
        self.dock[c_to].append(self.dock[c_from].pop())

    def move_operation(self, move_op: str):
        op = move_op.split(' ')
        amount, c_from, c_to = tuple(map(int, op[1::2]))

        for _ in range(amount):
            self.move_from_to(c_from - 1, c_to - 1)

    def load_row(self, row: str):
        for i, new_crate in enumerate(row[1::4]):
            if new_crate != ' ':
                self.dock[i].append(new_crate)

    def __repr__(self):
        return str(self.dock)


if __name__ == '__main__':
    with open('input', 'r') as f:
        data = f.read()

    split_data = data[:-1].split('\n\n')
    crates = split_data[0].split('\n')
    operations = split_data[1].split('\n')
    
    crane = Crane(int(max(crates[-1].split(' '))))

    list(map(crane.load_row, reversed(crates[:-1])))
    list(map(crane.move_operation, operations))

    print(''.join([area[-1] for area in crane.dock]))

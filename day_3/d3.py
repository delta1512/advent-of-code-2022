from string import ascii_letters as al

CHAR_MAP = {c : al.index(c) + 1 for c in al}


def get_data():
    with open('input', 'r') as f:
        for l in f.readlines():
            yield l


def get_score(rucksack: str):
    r_len = len(rucksack)
    common_l = (set(rucksack[:r_len//2]) & set(rucksack[r_len//2:])).pop()
    return CHAR_MAP.get(common_l)


if __name__ == '__main__':
    print(sum(map(get_score, get_data())))

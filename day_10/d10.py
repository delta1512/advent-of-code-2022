
def compile_cycles(ins: list[str]):
    '''
    Since addx takes 2 cycles, it is the same as noop followed by addx
    '''
    for i in ins:
        if i != 'noop':
            yield 'noop'

        yield i


def get_next_state(ins: list[str]):
    x = 1

    for i, mnu in enumerate(ins):
        yield i + 1, x

        if mnu != 'noop':
            x += int(mnu.split(' ')[1])


if __name__ == '__main__':
    with open('input', 'r') as f:
        data = f.read()

    print(list(get_next_state(compile_cycles(data[:-1].split('\n'))))[:12])

    print([x * i for i, x in get_next_state(compile_cycles(data[:-1].split('\n'))) if (i + 20) % 40 == 0])

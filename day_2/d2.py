from collections import Counter, deque
from itertools import chain

if __name__ == '__main__':
    with open('input', 'r') as f:
        data = f.read()

    print(sum(map(lambda tup: next(map(lambda l: dict(chain(*[([(' '.join(game), i + ord(game[1]) - 87) for game in zip(l[0], l[1])], l[1].rotate(1))[0] for i in [3, 0, 6]])), [[['A', 'B', 'C'], deque(['X', 'Y', 'Z'])]]))[tup[0]] * tup[1], Counter(data[:-1].split('\n')).items())))

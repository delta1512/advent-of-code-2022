package main

import (
  "fmt"
  "os"
  "strings"
  "time"
)


type Marker struct {
  pos int
  marker string
}


func Isunique(input string) bool {
  strArr := strings.Split(input, "")

  // This is intentionally slow
  for i := 0; i < 4; i++ {
    for j := 0; j < 4; j++ {
      if strArr[i] == strArr[j] && i != j {
        return false
      }
    }
  }

  return true
}


func Findbestmarker(markers chan Marker) {
  fmt.Println("Started Findbestmarker")
  current := Marker{999999, "1234"}
  best := current

  for ; current.pos >= 0; current = <-markers {
    fmt.Println(current.pos)
    fmt.Println(current.marker)

    if Isunique(current.marker) {
      if current.pos < best.pos {
        best = current
      }
    }
  }

  fmt.Println("Result is ", best.marker, " at ", best.pos)
}


func main() {
  f, err := os.Open("input")
  markers := make(chan Marker)

  if (err != nil) {
    return
  }

  go Findbestmarker(markers)

  go func() {
    fmt.Println("Started func")

    n := 4

    for i := 0; n == 4; i++ {
      f.Seek(int64(i), 0)

      ba := make([]byte, 4)

      cn, err := f.Read(ba)
      n = cn

      if err != nil {
        fmt.Println("Failed read")
        return
      }

      if n == 4 {
        markers <- Marker{i + 4, string(ba)}
      }
    }

    markers <- Marker{-1, ""}

    f.Close()
  }()

  fmt.Println("Started main")

  time.Sleep(1000 * time.Millisecond)
}

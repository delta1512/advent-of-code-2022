if __name__ == '__main__':
    with open('input', 'r') as f:
        data = f.read()

    print(max(map(lambda c: sum(map(int, c)), [cals.split('\n') for cals in data[:-1].split('\n\n')])))

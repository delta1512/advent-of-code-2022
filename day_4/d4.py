import csv

def get_data():
    with open('input', 'r') as f:
        csvf = csv.reader(f)

        for l in csvf:
            yield l


def str_to_range(s: str):
    bounds = s.split('-')
    return range(int(bounds[0]), int(bounds[1]) + 1)


def range_overlaps(a: range, b: range):
    s_a = set(a)
    s_b = set(b)

    return s_a >= s_b or s_a <= s_b


def process_line(l: list[str]):
    return range_overlaps(str_to_range(l[0]), str_to_range(l[1]))


if __name__ == '__main__':
    s = 0

    for i in filter(lambda x: x, map(process_line, get_data())):
        s += 1

    print(s)

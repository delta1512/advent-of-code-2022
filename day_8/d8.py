import itertools as it

import numpy


def is_visible(x: int, y: int, row: numpy.ndarray, col: numpy.ndarray):
    '''
    A tree is assumed to be invisible unless proven otherwise
    '''
    trivial = any((
        x == 0,
        y == 0,
        x == len(row) - 1,
        y == len(col) - 1
    ))

    if trivial:
        return True

    target = row[y] # We could also do col[x]

    # Ray-trace from the edges of the row...
    if max(row[:y]) < target or max(row[y+1:]) < target:
        return True

    # ...then the column
    if max(col[:x]) < target or max(col[x+1:]) < target:
        return True

    return False


if __name__ == '__main__':
    with open('input', 'r') as f:
        data = f.read()

    tree_rows = numpy.array([list(map(int, row)) for row in data[:-1].split('\n')])
    tree_cols = tree_rows.transpose()
    width = len(tree_rows[0])
    height = len(tree_cols[0])

    print([is_visible(x, y, tree_rows[x], tree_cols[y]) for x, y in it.product(range(tree_rows.shape[0]), range(tree_cols.shape[0]))].count(True))
